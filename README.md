#Basic `git` commands

## Commands list

-`git init`: create a local repo

-`git config`: store config info such as name, email, etc..

-`git status`: status of files

-`git add`: stage files for tracking

-`git commit`: create a new version

-`git log`: version info



## Local Repo

-`git init`

-`git add`
    
-`git diff`
    
-`git status`

-`git commit`

-`git log`

## Remote to Local
    
-`git clone`
    
-`git add`

-`git diff`

-`git status`

-`git commit`

-`git log`

-`git push`

-`git pull or git fetch + git merge`

## Map local and remote
    
-`git init`

-`git remote add origin remote_url`

-`git add`

-`git diff`

-`git status`

-`git commit`

-`git log`

-`git push`

-`git pull or git fetch + git merge`
